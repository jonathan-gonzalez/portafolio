@extends('layout.layout')

@section('title','portfolio')

@section('content')
    <h1>Crear Proyecto</h1>
<form method="POST" action="{{ route('projects.store')}}">
    @csrf
        <label>
            Titulo del proyecto <br>
                <input type="text" name="title">
        </label>
        <br>
        <label>
            URL del proyecto <br>
                <input type="text" name="url">
        </label>
        <br>
        <label>
            Descripcion del proyecto<br>
                <input type="text" name="description">
        </label>
        <br>
        <button>Guardar</button>
    </form>
   
@endsection